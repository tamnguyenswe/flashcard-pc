import requests
import json
import bs4 as bs
import os

def write_file(file_name, data):
	with open(file_name, 'w') as f:
		f.write(data)

rq	=	requests.Session()

page 	=	rq.get("https://www.cram.com/flashcards/wortschatz-3-9857680")

soup	=	bs.BeautifulSoup(page.text, 'lxml')


# print(soup.find(id = 'flashCardsListingTable'))

table_name 	=	soup.find(itemprop	=	"name").text

card_table	=	soup.find(id = 'flashCardsListingTable')

card_list	=	[]

for card in card_table.find_all('tr'):
	front		=	card.find(class_ = 'front_text card_text').text.strip()
	hint		=	card.find(class_ = 'hint_text card_text').text.strip()
	back		=	card.find(class_ = 'back_text card_text')
	back_vi		=	back.find_all('p')[0].text
	try:
		back_eng	=	back.find_all('p')[1].text
	except:
		back_eng	=	''

	back 		=	f'{back_vi}\n{back_eng}'

	card_list.append({
						'front'	: front, 
						'hint'	: hint, 
						'back'	: back
					})

if not os.path.exists('./Card Sets/'):
    os.makedirs('./Card Sets/')

with open(f'./Card Sets/{table_name}.json', 'w') as f:
	# for card in card_list:
	# 	f.write(str(card) + '\n')
	f.write(json.dumps(card_list, ensure_ascii=False, indent = 4))
	# f.write(str(card_list))

# with open('cards.html', 'w') as f:
# 	for card in card_list:
# 		f.write(f'Front:{card.front}\n')
# 		f.write(f'Hint:{card.hint}\n')
# 		f.write(f'Back:{card.back_vi}/{card.back_eng}\n')
# 		f.write('\n\n')

# first_card	=	Flashcard()
# card_1		=	card_table.find_all('tr')[0]






# back		=	card_1.find(class_ = 'back_text card_text')
# back_vi		=	back.find_all('p')[0].text
# back_eng	=	back.find_all('p')[1].text
# front		=	card_1.find(class_ = 'front_text card_text').text.strip()
# hint		=	card_1.find(class_ = 'hint_text card_text').text.strip()
# first_card.update(back_vi = back_vi, back_eng = back_eng)

# write_file('cards.html', card_table.prettify())

# print(hint)

# write_file('cards.html', card_1.)

# with open('out.html', 'w') as f:
# 	f.write(page.text)